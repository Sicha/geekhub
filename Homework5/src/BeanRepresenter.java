import java.lang.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class BeanRepresenter {
	private ArrayList Used = new ArrayList<Object>();
	
	public <T> void Representer(Object student, int n) throws IllegalArgumentException, IllegalAccessException{
		Class clazz = student.getClass();
		Field[] fields = clazz.getDeclaredFields();
		 
		for(Field cField: fields){
			if ( Used.contains(cField) ) break;
			Used.add(cField);
			cField.setAccessible(true);
			Class type = cField.getType();
			if (type == String.class){
				for (int i = 0; i < n; i++)
					System.out.print(" 	");
				System.out.println(cField.getName()+' '+cField.get(student));
			}
			else if (type.isPrimitive()){
				for (int i = 0; i < n; i++)
					System.out.print(" 	");
				System.out.println(cField.getName() +' '+ cField.get(student));
			}
			else{
				Representer(cField, ++n);
				
			}
			
			
		}
		Used.clear();
	}
	
}
