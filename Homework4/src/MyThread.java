import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MyThread extends Thread {
		private int begin;
		private int end;
		private String mask;
		private ArrayList<File> MyList;
		
		public MyThread(int arg1, int arg2, String arg3, ArrayList<File> arg4){
			begin = arg1;
			end = arg2;
			mask = arg3;
		}
		
		public MyThread() {
			
		}

		public void run() {
			for ( int i = begin; i <= end; i++){
				try {
					if(MyList.get(i).getCanonicalPath().endsWith(mask)){
						System.out.println(MyList.get(i).getCanonicalPath());
						System.out.flush();
					}
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}