import java.io.IOException;
import java.net.*;


public class Serwer {
	
	static public int ServerPort = 3339;
	public static byte[] buffer = new byte[1024];
	public static DatagramSocket ds;
	
	
	public static void main(String arg[]) throws IOException{
		int pos = 0;
		ds = new DatagramSocket(ServerPort);
		while(true)	{
			int c = System.in.read();
			switch (c){
			case -1:
				System.out.println("Serwer close.");
				System.out.flush();
				ds.close();
				return;
			case '\n':
				DatagramPacket arg0 = new DatagramPacket(buffer, pos, InetAddress.getLocalHost(), Client.ClientPort);
				ds.send(arg0);
				pos = 0;
				break;
				default:
					buffer[pos++] = (byte)c;
			}
		}
	}
	
}
