import java.io.IOException;
import java.net.*;


public class Client {
	static public int ClientPort = 4994;
	public static byte[] buffer = new byte[1024];
	public static DatagramSocket ds;
	
	
	static public void main(String arg[]) throws IOException{
		ds = new DatagramSocket(ClientPort);
		while(true){
			DatagramPacket p = new DatagramPacket(buffer, buffer.length);
			 ds.receive(p);
			System.out.println(new String(p.getData(), 0, p.getLength()));
			System.out.flush();
		}
	}
}
