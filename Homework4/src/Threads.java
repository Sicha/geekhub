import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/*  */

public class Threads {
	static ArrayList<File> MyList = new ArrayList<File>();
	
	public static void search(File begin){
		File[] use = begin.listFiles();
		int i = 1;
		while(i<use.length){
			if (use[i-1].isDirectory()){
				search(use[i-1]);
			}
			else{
				MyList.add(use[i-1]);
			}
			i++;
		}
	}
	
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	
	
	
	public static void main(String arg[]) throws IOException{
		String begin = in.readLine();
		String mask = in.readLine();
		File file = new File(begin);
		search(file);
		MyThread arg1 = new MyThread(0, MyList.size()/2, mask, MyList);
		MyThread arg2 = new MyThread(MyList.size()/2+1, MyList.size(), mask, MyList);
		arg1.run();
		arg2.run();
	}
}
