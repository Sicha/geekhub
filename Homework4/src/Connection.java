import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.regex.*;


public class Connection {
	static ArrayList<String> List2xx = new ArrayList<>();
	static ArrayList<String> List3xx = new ArrayList<>();
	static ArrayList<String> List4xx = new ArrayList<>();
	
	public static void Connect() throws IOException{
		String string;
		URLConnection conn = new URL("http://hi-tech.mail.ru/news/").openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		try{
			while((string = in.readLine()) != null) {
				Pattern patt = Pattern.compile("<a href=.*[^>]>.*");
				Matcher mat = patt.matcher(string);
				if(mat.matches()){
					int end =string.indexOf('>');
					String string1 = string.substring(mat.start(), end+1).replaceAll("<a href=\"(.*[^>])\">.*", "$1");
					HttpURLConnection connection = (HttpURLConnection) new URL(string1).openConnection();
					if ( connection.getResponseCode() >= 200 && connection.getResponseCode() < 300 ){
						List2xx.add(string1);
					}
					else if( connection.getResponseCode() >= 300 && connection.getResponseCode() < 400 ){
						List3xx.add(string1);
					}
					else List4xx.add(string1);
				}
			}
			System.out.println("2xx: "+List2xx);
			System.out.println("3xx: "+List3xx);
			System.out.println("4xx - 5xx: "+List4xx);
		}
		catch(IOException exception){
			System.out.println(exception.getMessage());
			return;
		}
		finally{
			in.close();
		}
	}
	
	public static void main(String arg[]) throws IOException{
		Connect();
	}
			
}
