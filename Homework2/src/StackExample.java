
public class StackExample {
	public static void main(String arg1[])
	{
		Stack MyStack = new Stack();
		MyStack.add(10);
		MyStack.add(20);
		MyStack.add(30);
		MyStack.add(40);
		MyStack.add(50);
		System.out.println(MyStack.peek());
		MyStack.poll();
		System.out.println(MyStack.peek());
		MyStack.remove();
		System.out.println(MyStack.peek());
	}
}
