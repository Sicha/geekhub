
public class Cars implements Comparable<Cars>{
	private String Mark;
	private int Length;
	private String Color;
	
	public String out()
	{
		return "Mark: "+Mark+"; Length: "+Length+"; Color: "+Color+".";
	}
	
 	public Cars(String mark, int length, String color)
	{
		Mark = mark;
		Length = length;
		Color = color;
	}

	@Override
	public int compareTo(Cars arg0) {
		if (Color.compareTo(arg0.Color) == 0)
		{
			if (Mark.compareTo(arg0.Mark) == 0)
			{
				if (Length < arg0.Length) return -1;
				else if (Length > arg0.Length) return 1;
				else return 0;
			}
			else return Mark.compareTo(arg0.Mark);
		}
		else return Color.compareTo(arg0.Color);
	}
	
	private int Mycompare(String arg0, String arg1)
	{
		int i = 0, j = 0;
		while ( i < arg0.length()-1 || j < arg1.length()-1 )
		{
			if ( arg0.charAt(i) < arg1.charAt(j) ) return -1;
			else if ( arg0.charAt(i) > arg1.charAt(j) ) return 1;
		}
		return 0;
	}
	
	
	
}
