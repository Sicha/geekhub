import java.util.*;

public class Stack implements Queue{
	LinkedList MyList = new LinkedList<>();

	@Override
	public boolean addAll(Collection arg0) {
		return MyList.addAll(arg0);
	}

	@Override
	public void clear() {
		MyList.clear();		
	}

	@Override
	public boolean contains(Object arg0) {
		return MyList.contains(arg0);
	}

	@Override
	public boolean containsAll(Collection arg0) {
		return MyList.containsAll(arg0);
	}

	@Override
	public boolean isEmpty() {
		return MyList.isEmpty();
	}

	@Override
	public Iterator iterator() {
		return MyList.iterator();
	}

	@Override
	public boolean remove(Object arg0) {
		return MyList.removeLastOccurrence(arg0);
	}

	@Override
	public boolean removeAll(Collection arg0) {
		return MyList.removeAll(arg0);
	}

	@Override
	public boolean retainAll(Collection arg0) {
		return MyList.retainAll(arg0);
	}

	@Override
	public int size() {
		return MyList.size();
	}

	@Override
	public Object[] toArray() {
		return MyList.toArray();
	}

	@Override
	public Object[] toArray(Object[] arg0) {
		return MyList.toArray(arg0);
	}

	@Override
	public boolean add(Object arg0) {
		return MyList.add(arg0);
	}

	@Override
	public Object element() {
		return MyList.element();
	}

	@Override
	public boolean offer(Object arg0) {
		return MyList.offer(arg0);
	}

	@Override
	public Object peek() {
		return MyList.peekLast();
	}

	@Override
	public Object poll() {
		return MyList.pollLast();
	}

	@Override
	public Object remove() {
		return MyList.removeLast();
	}
	
	
}
