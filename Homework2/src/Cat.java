class Cat {
	int[] rgbColor = new int[3];
	int age;
	
	Cat()
	{
		rgbColor[0] = 0;
		rgbColor[1] = 0;
		rgbColor[2] = 0;
		age = 0;
	}
	
	Cat( int red, int green, int blue, int Age )
	{
		rgbColor[0] = red;
		rgbColor[1] = green;
		rgbColor[2] = blue;
		age = Age;
	}
	
	public int HashCode()
	{
		return rgbColor[0]*11*11*11 + rgbColor[1]*11*11 + rgbColor[2]*11 + age;
	}
	
	public String ToString()
	{
		return "Color: red = "+ this.rgbColor[0] + "; green = " + this.rgbColor[1] + "; blue = " + this.rgbColor[2] + ". Age = " + this.age;
	}
	
	@Override
	public boolean equals( Object MyCat )
	{ 
		if ( MyCat instanceof Cat ){
			Cat MyNewCat = (Cat)MyCat;
			if ( this.age > MyNewCat.age ) return false; 
			else if ( this.age > MyNewCat.age ) return false;
			else {
				for (int i = 0; i < 3; i++)
				{
					if ( this.rgbColor[i] > MyNewCat.rgbColor[i] ) return false;
					else if ( this.rgbColor[i] > MyNewCat.rgbColor[i] ) return false;
				}
			}
		}
		return true;
	}
	
}

