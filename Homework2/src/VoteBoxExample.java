
public class VoteBoxExample {
	public static void main(String arg[])
	{
		VoteBox box = new VoteBox("Petrov", "Ivanov");
		box.add("Petrov");
		box.add("Ivanov");
		box.add("Shevchenko");
		box.add("Gusev");
		box.add("Petrov");
		box.add("Ivanov");
		box.add("Ivanov");
		VoteBox box2 = new VoteBox("Ivanov", "Petrov");
		box2.add("Petrov");
		box2.add("Ivanov");
		box2.add("Shevchenko");
		box2.add("Gusev");
		box2.add("Petrov");
		box2.add("Ivanov");
		box2.add("Ivanov");
		System.out.println("box1.compareTo(box2) = " + box.compareTo(box2));
		System.out.println("Box1:");
		while(!box.MyVoteBox.isEmpty())
		{
			System.out.println(box.MyVoteBox.remove());
		}
		System.out.println("Box2:");
		while(!box2.MyVoteBox.isEmpty())
		{
			System.out.println(box2.MyVoteBox.remove());
		}
	}
}
