class ExceptionSon extends Exception {
	private int Errornum;
	public ExceptionSon(int num)
	{
		Errornum = num;
	}
	public String ToString()
	{
		return "Exception son error at number "+Errornum;
	}	
}

class RuntimeExceptionSon extends RuntimeException {
	private int Errornum;
	public RuntimeExceptionSon(int num)
	{
		Errornum = num;
	}
	public String ToString()
	{
		return "Runtime exception son error at number "+Errornum;
	}
}


public class Main {
	static void chek1(int num) throws ExceptionSon
	{
		if ( num == 15 )
		{
			throw new ExceptionSon(num);
		}
		System.out.println("1:"+num);
	}
		
	static void chek2(int num) //throws RuntimeExceptionSon
	{
		if ( num == 5 )
		{
			throw new RuntimeExceptionSon(num);
		}
		System.out.println("2:"+num);
	}
	
	public static void main(String arg[])
	{
		for ( int i = 0; i < 20; i++ )
		{
			try
			{
				chek1(i);
				chek2(i);
			}
			catch(ExceptionSon e)
			{
				System.out.println("Catch "+e.ToString());
			}
			catch(RuntimeExceptionSon e)
			{
				System.out.println("Catch "+e.ToString());
			}
		}
	}

}
