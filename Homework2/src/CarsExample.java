import java.util.*;
import java.lang.*;

public class CarsExample {
	public static void main(String arg[])
	{
		LinkedList<Cars> Garage = new LinkedList<Cars>();
		Garage.add(new Cars("Audi", 11, "Green"));
		Garage.add(new Cars("Mazda", 18, "Red"));
		Garage.add(new Cars("BMW", 17, "White"));
		Collections.sort(Garage);
		while( !Garage.isEmpty() )
		{
			int i = 1;
			System.out.println("Car "+i+": "+Garage.poll().out());
			i++;
		}
		System.out.flush();
	}
}
