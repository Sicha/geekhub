import java.util.*;

public class VoteBox implements Comparable<VoteBox> {
	LinkedList MyVoteBox = new LinkedList<>();
	String name1;
	String name2;
	
	public VoteBox( String n1, String n2 )
	{
		name1 = n1;
		name2 = n2;
	}
	
	public boolean add(String vote)
	{
		if ( name1.equals(vote) ) return false;
		if ( name2.equals(vote) )
		{
			MyVoteBox.add(vote);
			return MyVoteBox.add(vote);
		}
		return MyVoteBox.add(vote);
	}

	@Override
	public int compareTo(VoteBox arg0) {
		if ( MyVoteBox.size() - arg0.MyVoteBox.size() < 0 ) return -1;
		else if ( MyVoteBox.size() - arg0.MyVoteBox.size() > 0 ) return 1;
		else return 0;
	}
}



