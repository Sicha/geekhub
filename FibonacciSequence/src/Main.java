import java.io.BufferedReader;
import java.nio.Buffer;
import java.util.Scanner;

public class Main {
	static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        int n;
        n = in.nextInt();
        int f1 = 0, f2 = 1;
        if (n>0) System.out.print(f1+" ");
        if (n>1) System.out.print(f2+" ");
        n -= 2;
        for (int i = 0; i < n; i++)
        {
            f2 += f1;
            f1 = f2-f1;
            System.out.print(f2+" ");
        }
    }


}
