import java.io.*;


public class Main 
{
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	static PrintWriter out = new PrintWriter( System.out );
	
	static public void main( String arg[] ) throws IOException
	{
		int N = Integer.parseInt( in.readLine() );
		for ( int i = 1; i <= N; i++ )
		{
			out.print(1.0/i+" ");
		}
		out.flush();
	}
}
