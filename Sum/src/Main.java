import java.io.*;
import java.util.*;

public class Main {
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	static PrintWriter out = new PrintWriter( System.out );
	
	static int FromSymbol(char arg1)
	{
		if ( arg1 >= '0' && arg1 <= '9' ) return arg1-'0';
		else return arg1-'A'+10;
	}
	
	static int SearchPoint ( String arg1 )
	{
		for ( int i = 0; i < arg1.length()-1; i++ )
			if ( arg1.charAt(i) == '.' ) return i;
		return arg1.length();
	}
	
	static double ToDouble(String arg1, int arg2, int arg3)
	{
		double sum = 0;
		int from;
		if ( arg3 == 16 ) from = 2;
		else if ( arg3 == 8 ) from = 1;
		else from = 0;
		for ( int i = arg2-1, k = 0; i >= from; i--, k++ )
			sum += FromSymbol(arg1.charAt(i)) * Math.pow(arg3, k);
		if ( arg2 != arg1.length() )
		{
			for ( int i = arg2+1, k = 1; i <= arg1.length()-1; i++, k++ )
				sum+=FromSymbol(arg1.charAt(i))*Math.pow(arg3, -k);
		}
		return sum;
	}
	
	static double FromString (String arg1)
	{
		double sum = 0;
		boolean negative = false;
		if ( arg1.charAt(0) == '-' )
		{
			negative = true;
			arg1 = arg1.substring(1, arg1.length());
		}
		if ( arg1.charAt(0) == '0' )
		{
			if ( arg1.charAt(1) == 'x' )
			{
				sum = ToDouble(arg1, SearchPoint(arg1), 16);
			}
			else
			{
				sum = ToDouble(arg1, SearchPoint(arg1), 8);
			}
		}
		else
		{
			sum = Double.valueOf(arg1);
		}
		if (negative) return -sum;
		else return sum;
	}
	
	static public void main(String arg[])throws IOException
	{
		StringTokenizer input = new StringTokenizer(in.readLine());
		String num1str = input.nextToken();
		String num2str = input.nextToken();
		double num1dou, num2dou;
		num1dou = FromString(num1str);
		num2dou = FromString(num2str);
		out.println(num1dou+num2dou);
		out.flush();
	}

}
