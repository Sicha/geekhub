import java.io.*;

public class Copying {
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	static PrintWriter out = new PrintWriter( System.out );
	
	static public void main(String arg[]) throws IOException
	{
		String from = in.readLine();
		String to = in.readLine();
		String buf = in.readLine();
		if ( buf.equalsIgnoreCase("with") )
		{
			File FileFrom = new File(from);
			File FileTo = new File(to);
			BufferedInputStream input = new BufferedInputStream(new FileInputStream(FileFrom));
			BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(FileTo));
			try
			{
				long begin = System.currentTimeMillis();
				int c = 0;
				while( (c = input.read()) >= 0 )
					output.write(c);
				output.flush();
				long end = System.currentTimeMillis();
				out.println("Time elapsed - " + (begin-end));
			}
			catch(IOException exception)
			{
				out.println(exception.getMessage());
				out.flush();
				return;
			}
			finally
			{
				input.close();
				output.close();				
			}
		}
		else
		{
			InputStream input = new FileInputStream(from);
			OutputStream output = new FileOutputStream(to);
			try
			{
				long begin = System.currentTimeMillis();
				int c = 0;
				while( (c = input.read()) >= 0 )
					output.write(c);
				output.flush();
				long end = System.currentTimeMillis();
				out.println("Time elapsed - " + (begin-end));
			}
			catch(IOException exception)
			{
				out.println(exception.getMessage());
				out.flush();
				return;
			}
			finally
			{
				input.close();
				output.close();
			}
		}
		out.flush();
	}
	
}
