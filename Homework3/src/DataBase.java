
import java.io.*;
import java.sql.*;
import java.util.*;

public class DataBase {
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	static PrintWriter out = new PrintWriter( System.out );
	
	static Connection connection() throws IOException, SQLException 
	{
		String url = "jdbc:mysql://";
		out.print("Write host: ");
		out.flush();
		String host = in.readLine().toString();
		out.print("Write db: ");
		out.flush();
		String db = in.readLine().toString();
		out.print("Write login: ");
		out.flush();
		String login = in.readLine().toString();
		out.print("Write password: ");
		out.flush();
		String password = in.readLine().toString();
		Connection con = null;
		try
		{
			con = DriverManager.getConnection(url+host+'/'+db, login, password);
		}
		catch(SQLException exception)
		{
			out.println(exception.getMessage());
			out.flush();
		}
		return con;
	}
	
	static boolean IfSelect( String arg1 )
	{
		StringTokenizer str = new StringTokenizer(arg1);
		String token = str.nextToken();
		return token.equalsIgnoreCase("SELECT");
	}
	
	static void command(Connection con ) throws IOException, SQLException
	{
		out.print("Write command: ");
		out.flush();
		String com = in.readLine().toString();
		Statement statement = con.createStatement();
		if (IfSelect(com)) 
		{
			ResultSet resultset;
			try
			{
				resultset = statement.executeQuery( com );
			}
			catch(SQLException exception)
			{
				out.println(exception.getMessage());
				return;
			}
			ResultSetMetaData RSMD = resultset.getMetaData();
			int N = RSMD.getColumnCount();
			int[] Max = new int[N];
			for ( int i = 1; i <= N; i++ )
			{
				Max[i-1] = RSMD.getCatalogName(i).length();
			}
			resultset.beforeFirst();
			while ( resultset.next() )
			{
				for ( int i = 0; i < N; i++ )
				{
					Max[i] = Math.max(Max[i], resultset.getString(i+1).length());
				}
			}
			out.println();
			out.print('|');
			for ( int i = 1; i <= N; i++ )
			{
				String name = RSMD.getColumnName(i);
				int k = (Max[i-1]-name.length());
				for (int j = 0; j < k/2; j++)
				{
					out.print(' ');
				}
				out.print(name);
				for (int j = 0; j < k/2 + ((k%2 == 1)?1:0); j++)
				{
					out.print(' ');
				}
				out.print('|');
			}
			out.println();
			int sum = 0;
			for ( int i = 0; i < N; i++ )
			{
				sum += Max[i];
			}
			sum += N + 1;
			for ( int i = 0; i < sum; i++ )
			{
				out.print('-');
			}
			out.println();
			resultset.beforeFirst();
			while( resultset.next() )
			{
				out.print('|');
				for ( int i = 1; i <= N; i++ )
				{
					String data = resultset.getString(i);
					int k = ( Max[i-1] - data.length() );
					for ( int j = 0; j < k/2; j++ )
					{
						out.print(' ');
					}
					out.print(data);
					for ( int j = 0; j < k/2 + ((k%2 == 1)?1:0); j++ )
					{
						out.print(' ');
					}
					out.print('|');
				}
				out.println();
			}
		}
		else 
		{
			int k;
			try
			{
				k = statement.executeUpdate(com);
			}
			catch(SQLException exception)
			{
				out.println(exception.getMessage());
				return;
			}
			out.println( "Lines Changed - "+k );
		}
		out.flush();
	}
	
	static public void main(String asr[]) throws IOException, SQLException
	{
		Connection con;
		while(true)
		{
			con = connection();
			if ( con == null )
			{
				out.println("Try again.");
				out.flush();
			}
			else break;
		}
		while(true)
		{
			command(con);
			out.println("Do you want to continue (Y/N) ?");
			out.flush();
			String Yes = in.readLine();
			if ( Yes.charAt(0) == 'N' )
				{
					out.println("Bye");
					out.flush();
					break;
				}
		}
		
		
	}
	
}
