import java.io.*;

public class Encoding {
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	static PrintWriter out = new PrintWriter( System.out );
	
	static public void main(String arg[]) throws IOException
	{
		String f1 = in.readLine();
		String cod1 = in.readLine();
		String f2 = in.readLine();
		String cod2 = in.readLine();
		Reader reader = new InputStreamReader( new FileInputStream(f1), cod1 );
		Writer writer = new OutputStreamWriter( new FileOutputStream(f2), cod2 );
		try
		{
			int c = 0;
			while( (c = reader.read()) >= 0 )
				writer.write(c);
		}
		catch(UnsupportedEncodingException exception)
		{
			out.println(exception.getMessage());
			return;
		}
		finally
		{
			reader.close();
			writer.close();
		}
	}
}
