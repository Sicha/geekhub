import java.io.*;

public class Folder {
	static BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
	static PrintWriter out = new PrintWriter( System.out );
	
	public static void main(String arg[]) throws IOException
	{
		String command;
		command = in.readLine();
		String way;
		way = in.readLine();
		String name;
		name = in.readLine();
		if ( command.equalsIgnoreCase("create") )
		{
			File file = new File(way+"\\"+name);
			if (file.mkdir())
			{
				out.println("Folder created.");
			}
			else
			{
				out.println("Folder has not been created.");
			}
		}
		else if ( command.equalsIgnoreCase("delete") )
		{
			File file = new File(way+"\\"+name);
			if ( file.delete() )
			{
				out.println("Folder deleted.");
			}
			else 
			{
				out.println("Fplder has not been deleted.");
			}
		}
		else if ( command.equalsIgnoreCase("rename") )
		{
			String newname = in.readLine();
			File file = new File(way+"\\"+name);
			if (file.renameTo(new File(newname)))
			{
				out.println("Folder renamed.");
			}
			else 
			{
				out.println("Folder has not been renamed.");
			}
		}
		out.flush();
	}
	
}
