package ActionTest;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ActionTest
 */
public class ActionTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActionTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String name;
		String value;
		
		HttpSession session = request.getSession();
		
		
		if("add".equalsIgnoreCase(action) || "update".equalsIgnoreCase(action)){
			name = request.getParameter("name");
			value = request.getParameter("value");
			session.setAttribute(name, value);
		}else if("remove".equalsIgnoreCase(action)){
			name = request.getParameter("name");
			value = request.getParameter("value");
			session.removeAttribute(name);
		}else if("invalidate".equalsIgnoreCase(action)){
			session.invalidate();
		}
		
		String Resultstr = "";
		Enumeration<String> value1 = session.getAttributeNames();
		while(value1.hasMoreElements()){
			String key = (String) value1.nextElement();
			String val = (String) session.getAttribute(key);
			Resultstr += key + " " + val + "<br/>";
		}
		
		String MyForm = "<form action=\"./ActionTest\" method=\"get\"> <p>action: <input type=\"text\" name=\"action\" /> name: <input type=\"text\" name=\"name\"> value: <input type=\"text\" name=\"value\"></p> <p><input type=\"submit\" value=\"OK\"></p> </form>";
		
		
		response.getWriter().write(MyForm);
		response.getWriter().write(Resultstr);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
