import java.io.*;
import java.util.StringTokenizer;


public class Main {
	static BufferedReader in = new BufferedReader( new InputStreamReader(System.in) );
	static PrintWriter out = new PrintWriter( System.out );
	static public void main( String arg[] ) throws IOException
	{
		StringTokenizer input = new StringTokenizer( in.readLine() );
		int m = Integer.parseInt(input.nextToken());
		int n = Integer.parseInt(input.nextToken());
		for ( int i = m; i <= n; i++ )
		{
			out.print( (char)i + " " );
		}
		out.flush();
	}

}
